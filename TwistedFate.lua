if (myHero.charName ~= "TwistedFate") then return end
	local version = "1.13"
require "Prodiction"

local ComputerRunningWithHamster = false -- anti-lag, less checks when using Q
--Spell damages
local Qdamage = {60, 110, 160, 210, 260}
local Qscaling = 0.65
local Wdamage = {15, 22.5, 30, 37.5, 45}
local Wscaling = 0.4
local Edamage = { 55, 80, 105, 130, 155}
local Escaling = 0.4
local DFG, SHEEN, LICH= nil, nil, nil

--Spell data
local AArange = 650
local Rrange = 5500
local Qrange = 1450
local Qdelay = 250
local Qspeed =  1450
local Qradius = 80
local Qangle = 2*3.14159 * 0.07

local levelUps = {2, 1, 1, 3, 1, 4, 1, 2, 1, 2, 4, 2, 2, 3, 3, 4, 3, 3}
local onReadyFired = false

local CastingUltimate = false
local Qtarget = nil
local AAtarget = nil

--[[	Prodiction	]]
local ProdictionManager = nil
local QProdiction = nil

--[[Card Locker]]
local NONE, RED, BLUE, YELLOW = 0, 1, 2, 3
local COOLDOWNN, READYY, SELECTING, SELECTED = 0, 1, 2, 3
local Status = READYY
local CardColor = NONE
local ToSelect = NONE
local LastCard = NONE

local Sure = false
local Menu = nil
local StunnedTargets = {}
--[[	Killable texts and alerts	]]
local DamageToHeros = {}
local LastAlert = 0
local lastrefresh = 0

function OnLoad()
	autoLevelSetSequence(levelUps)		
	Menu = scriptConfig("ProTwisted Fate", "ProTwisted Fate")
	Menu:addParam("combo", "Select gold + use all items", SCRIPT_PARAM_ONKEYDOWN, false, 32)
	Menu:addSubMenu("Spells", "spells")

		Menu.spells:addSubMenu("Q (Wild Cards)", "Q")
			Menu.spells.Q:addParam("cast", "Cast Q using prodiction", SCRIPT_PARAM_ONKEYDOWN, false,   string.byte("T"))
			Menu.spells.Q:addParam("auto", "Auto Q inmobile enemies", SCRIPT_PARAM_ONOFF, true)
			Menu.spells.Q:addParam("auto2", "Auto Q if Q cant fail", SCRIPT_PARAM_ONOFF, true)
			
		Menu.spells:addSubMenu("W (Pick a card)", "W")
			Menu.spells.W:addParam("yellow", "Yellow Card", SCRIPT_PARAM_ONKEYDOWN, false,  string.byte("W"))
			Menu.spells.W:addParam("blue", "Blue Card", SCRIPT_PARAM_ONKEYDOWN, false,  string.byte("E"))
			Menu.spells.W:addParam("red", "Red Card", SCRIPT_PARAM_ONKEYDOWN, false,  string.byte("X"))
			Menu.spells.W:addParam("secure", "Secure the right card",  SCRIPT_PARAM_ONOFF, true)
			Menu.spells.W:addParam("autoaa", "Prevent AA while locking new card (Manual)",  SCRIPT_PARAM_ONOFF, true)
			Menu.spells.W:addParam("autoaa2", "Prevent AA while locking new card (Autocarry)",  SCRIPT_PARAM_ONOFF, true)
		
		Menu.spells:addSubMenu("R (Destiny)", "R")
			Menu.spells.R:addParam("auto", "Auto Lock Yellow card after using ultimate",  SCRIPT_PARAM_ONOFF, true)
			
	Menu:addSubMenu("Items", "items")
		Menu.items:addParam("usedfg", "Use dfg on target", SCRIPT_PARAM_ONOFF, true)
		Menu.items:addParam("waitdfg", "Wait for yellow card before using dfg",  SCRIPT_PARAM_ONOFF, true)
		
	Menu:addSubMenu("Drawing", "drawing")
		Menu.drawing:addParam("drawQ", "Draw Q range", SCRIPT_PARAM_ONOFF, true)
		Menu.drawing:addParam("drawRN", "Draw R range while casting ultimate", SCRIPT_PARAM_ONOFF, true)
		Menu.drawing:addParam("drawR", "Draw R range on minimap", SCRIPT_PARAM_ONOFF, true)
		Menu.drawing:addParam("alert", "Alert low health enemies", SCRIPT_PARAM_ONOFF, true)
		Menu.drawing:addParam("health", "Draw remaining health after combo", SCRIPT_PARAM_ONOFF, true)
	
	--	Set up prodiction	
	ProdictionManager = ProdictManager.GetInstance()
	QProdiction = ProdictionManager:AddProdictionObject(_Q, Qrange, Qspeed, Qdelay/1000, Qradius)

	PrintChat("<font color=\"#81BEF7\">ProTwistedFate (<b>"..version.."</b>) loaded successfully</font>")
end

function ComboDamage(target)
	local magicdamage = 0
	local phdamage = 0
	local truedamage = 0
	if DFG ~= 0 and (myHero:CanUseSpell(DFG)==READY) then
		m = 1.2
		truedamage = truedamage + myHero:CalcMagicDamage(target, target.maxHealth *0.15)
	else
		m = 1
	end
	if SHEEN ~= 0 then
		phdamage = phdamage + myHero.totalDamage - myHero.addDamage
	end
	
	if LICH ~= 0 then
		magicdamage  = magicdamage + 50 + 0.75 * myHero.ap
	end
	
	if (myHero:GetSpellData(_Q).level ~= 0)  and myHero:CanUseSpell(_Q) == READY  then
		magicdamage = magicdamage + Qdamage[myHero:GetSpellData(_Q).level]  + Qscaling * myHero.ap
	end
	
	if (myHero:GetSpellData(_W).level ~= 0) and myHero:CanUseSpell(_W) == READY then
		magicdamage = magicdamage + Wdamage[myHero:GetSpellData(_W).level]  + Wscaling * myHero.ap + myHero.totalDamage
	end
	
	if (myHero:GetSpellData(_E).level ~= 0)  then
		magicdamage = magicdamage + Edamage[myHero:GetSpellData(_E).level]  + Escaling * myHero.ap
	end
	phdamage = myHero.totalDamage
	
	if (IgniteSlot() ~= nil) and myHero:CanUseSpell(IgniteSlot()) == READY then
		truedamage = truedamage + 50 + 20 * myHero.level
	end
	
	return m * myHero:CalcMagicDamage(target, magicdamage) + myHero:CalcDamage(target, phdamage) + truedamage
end

function IgniteSlot()
	if myHero:GetSpellData(SUMMONER_1).name:find("SummonerDot") then
		return SUMMONER_1
	elseif myHero:GetSpellData(SUMMONER_2).name:find("SummonerDot") then
		return SUMMONER_2
	else
		return nil
	end
end
	
function GetBestTarget(Range)
	local LessToKill = 100
	local LessToKilli = 0
	local target = nil
	--	LESS_CAST	
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if ValidTarget(enemy, Range) then
			DamageToHero = myHero:CalcMagicDamage(enemy, 200)
			ToKill = enemy.health / DamageToHero
			if (ToKill < LessToKill) or (LessToKilli == 0) then
				LessToKill = ToKill
				LessToKilli = i
			end
		end
	end
	
	if LessToKilli ~= 0 then
		for i, enemy in ipairs(GetEnemyHeroes()) do
			if i == LessToKilli then
				target = enemy
			end
		end
	end
	return target
end

function OnProcessSpell(unit, spell)
	if unit.isMe then
		if spell.name == "PickACard" then
			Status = SELECTING
		end
	end
end

function OnTick()
	DFG, SHEEN, LICH = GetInventorySlotItem(3128) and  GetInventorySlotItem(3128) or 0, GetInventorySlotItem(3057) and GetInventorySlotItem(3057) or 0, GetInventorySlotItem(3100) and GetInventorySlotItem(3100) or 0
	Qtarget = GetBestTarget(Qrange)
	AAtarget = GetBestTarget(AArange)
	if (myHero:CanUseSpell(_W) == COOLDOWN) or (myHero:CanUseSpell(_W) == NOMANA) then Status = COOLDOWNN end
	if (((myHero:CanUseSpell(_W) == READY) and (Status == COOLDOWNN)) or myHero.dead) then Status = READYY end
	RefreshKillableTexts()
	CheckStunnedTargets()
	--Combo

	if Menu.combo then
		if Status == READYY then
			ToSelect = YELLOW
			CastSpell(_W)
		end
		if Status == SELECTED then
			if AAtarget ~= nil then
				if (DFG ~= 0) and (myHero:CanUseSpell(DFG)==READY) then
					CastSpell(DFG, AAtarget)
				end
			end
		end
	end
	--Spells
	--
	--Q
	if Menu.spells.Q.cast then
		--	Ask prodiction to get the predicted position	
		if Qtarget ~= nil then
			QProdiction:GetPredictionCallBack(Qtarget, ProdictionQCallback)
		end
	end
	
	if Menu.spells.Q.auto2 then
		for i, enemy in ipairs(GetEnemyHeroes()) do
			if ValidTarget(enemy, Qrange) then
				local Distance = GetDistance(enemy)
				local DistanceToMove = 80 + Qradius
				local TimeToMove = Distance / Qspeed + 0.25 --in seconds
				local RequiredMS = DistanceToMove / TimeToMove
				if enemy.ms < RequiredMS then
					if myHero:CanUseSpell(_Q) == READY then
						--CastSpell(_Q, enemy.x, enemy.z)
						QProdiction:GetPredictionCallBack(enemy, ProdictionQCallback)
					end
				end
			end
		end
	end
	
	--W
	if Menu.spells.W.yellow or Menu.spells.W.red or Menu.spells.W.blue then
		if Status == READYY then
				if  Menu.spells.W.yellow then
					ToSelect = YELLOW
				elseif Menu.spells.W.red then
					ToSelect = RED
				elseif Menu.spells.W.blue then
					ToSelect = BLUE
				end
				CastSpell(_W)
				
		end
	end
	
	if (Menu.spells.W.autoaa or Menu.spells.W.autoaa2) and (Status == SELECTING) then --Cancel the autoattacks while moving
		BlockAttacks = true
		if Menu.spells.W.autoaa2 then
			myHero:MoveTo(mousePos.x, mousePos.z)
		end
	elseif BlockAttacks then
		BlockAttacks = false
	end
	
	if not Menu.spells.W.secure then Sure = true end
	
	if (Sure) and (ToSelect == CardColor) and  (Status == SELECTING) then
		CastSpell(_W)
	end
	
	if Menu.drawing.alert then
		for i, enemy in ipairs(GetEnemyHeroes()) do
			if ValidTarget(enemy) then
				if (enemy.health < ComboDamage(enemy)) and ((GetTickCount() - LastAlert) > 30000)then
					PrintAlert("Enemy "..enemy.charName.." is killable!", 3, 255, 0, 0,nil)
					LastAlert = GetTickCount()
				end
			end
		end
	end
end

--	Prodiction calls this function when it finishes to calculate the predicted position 
function ProdictionQCallback(unit, pos, spell)
	local PredictedPosition = Vector(pos.x, 0, pos.z)
	local MyPosition = Vector(myHero.x, 0, myHero.z)
	local HitBox = 75 --Hitbox Sizes, not used yet
	
	if myHero:CanUseSpell(_Q) ~= READY then return end
	
	if CountEnemyHeroInRange(Qrange+300) == 1 then	
	local Rotate = math.random(1, 6) --4fun	
		--TODO: check minion/tower/wall block to increase the hit chance- maybe using a path finding algorithm between predicted point and target position
		
		if (Rotate < 3) then --feeling luck
			if Rotate == 1 then Qangle = -Qangle end
			LastPosition = MyPosition + (PredictedPosition - MyPosition):rotated(0,Qangle,0)
		else	
			LastPosition = PredictedPosition
		end
	else
		LastPosition = GetBestQPosition(unit, PredictedPosition, true)
	end
	if GetDistance(LastPosition) < Qrange+10 then
		CastSpell(_Q, LastPosition.x,  LastPosition.z)
	end
end


--	Checks the number of target hit by wildcards and returns the best option (Considering 3 the 3 cards), Getmec-like function to twisted fate Q
function GetBestQPosition(MainTarget, PredictedPosition, centered)
	local HitBox = 75 --Hitbox Sizes
	local PosibleCastPoints = {}
	local PosibleCastPoints2 = {}
	local Perpendicular = nil
	local MyPosition = Vector(myHero.x, 0, myHero.z)
		
	--Get the positions of the enemies
	local PredictedPositions ={}
	for i, enemy in ipairs(GetEnemyHeroes()) do
		if ValidTarget(enemy, Qrange+100) and (enemy.charName ~= MainTarget.charName) then
			local Position = QProdiction:GetPrediction(enemy)
			if Position == nil then
				Position = enemy
			end
			if (GetDistance(Vector(Position.x, 0, Position.z)) < Qrange) then--
				table.insert(PredictedPositions, Vector(Position.x, 0, Position.z))
			end
		end
	end
	
	Perpendicular = (HitBox + Qradius) * ((PredictedPosition - MyPosition):normalized():perpendicular())
	
	table.insert(PosibleCastPoints, PredictedPosition) --centered in target
	if not centered then
		table.insert(PosibleCastPoints, PredictedPosition + Perpendicular) -- just at the edge of the hitbox
		table.insert(PosibleCastPoints, PredictedPosition - Perpendicular) -- just at the edge of the hitbox to the other sides
	end
	
	if not ComputerRunningWithHamster then
		if not centered then
			table.insert(PosibleCastPoints, PredictedPosition + Perpendicular/2)
			table.insert(PosibleCastPoints, PredictedPosition - Perpendicular/2)
		end
		
		for _, Position in ipairs(PosibleCastPoints) do --MOAR Positions
			table.insert(PosibleCastPoints2, Position:rotated(0, Qangle, 0))
			table.insert(PosibleCastPoints2, Position:rotated(0, -Qangle, 0))
		end
		
		for _, Position in ipairs(PosibleCastPoints2) do
			table.insert(PosibleCastPoints, Position)
		end
	end
	


	local BestPosition = nil
	local BestHit = 0

	for _, Position in ipairs(PosibleCastPoints) do
		local hit = 0
		local EndPoints  = {Qrange * (Position - MyPosition):normalized(), Qrange * (Position - MyPosition):normalized():rotated(0, Qangle, 0), Qrange * (Position - MyPosition):normalized():rotated(0, -Qangle, 0)}
		for k, PredictedPos in ipairs(PredictedPositions) do
			local enemyhit = false
			for i, CurrentEndPoint in ipairs(EndPoints) do
				local temp, ProjectionToSegment, temp2 = VectorPointProjectionOnLineSegment(MyPosition, CurrentEndPoint, PredictedPos)					
				if GetDistance(ProjectionToSegment, PredictedPos) <= (HitBox + Qradius) then
					enemyhit = true
				end
			end
			if enemyhit then
				hit = hit + 1
			end
		end

		if (BestPosition == nil) or (hit > BestHit) then
			BestPosition = Position
			BestHit = hit
		end
	end

	return BestPosition
end

function ProdictionQCallback2(unit, pos, spell) --We are 100% sure that the unit will be in that position
	local PredictedPosition = Vector(pos.x, 0, pos.z)
	local MyPosition = Vector(myHero.x, 0, myHero.z)
	if (myHero:CanUseSpell(_Q) ~= READY) or not Menu.spells.Q.auto then return end
	
	if (CountEnemyHeroInRange(Qrange+300) > 1) then 
		LastPosition = GetBestQPosition(unit, PredictedPosition, false)
	else
		LastPosition = PredictedPosition
	end
	
	if GetDistance(LastPosition) <= Qrange+10 then
		CastSpell(_Q, LastPosition.x, LastPosition.z)
	end
end


--[[Update the bar texts]]
function RefreshKillableTexts()
	if ((GetTickCount() - lastrefresh) > 1000) and (Menu.drawing.health) then
		for i=1, heroManager.iCount do
			local enemy = heroManager:GetHero(i)
			if ValidTarget(enemy) then
				DamageToHeros[i] =  ComboDamage(enemy)
			end
		end
		lastrefresh = GetTickCount()
	end
end
	
--[[	Credits to zikkah	]]
function GetHPBarPos(enemy)
	enemy.barData = GetEnemyBarData()
	local barPos = GetUnitHPBarPos(enemy)
	local barPosOffset = GetUnitHPBarOffset(enemy)
	local barOffset = { x = enemy.barData.PercentageOffset.x, y = enemy.barData.PercentageOffset.y }
	local barPosPercentageOffset = { x = enemy.barData.PercentageOffset.x, y = enemy.barData.PercentageOffset.y }
	local BarPosOffsetX = 171
	local BarPosOffsetY = 46
	local CorrectionY =  0
	local StartHpPos = 31
	barPos.x = barPos.x + (barPosOffset.x - 0.5 + barPosPercentageOffset.x) * BarPosOffsetX + StartHpPos
	barPos.y = barPos.y + (barPosOffset.y - 0.5 + barPosPercentageOffset.y) * BarPosOffsetY + CorrectionY 
						
	local StartPos = Vector(barPos.x , barPos.y, 0)
	local EndPos =  Vector(barPos.x + 108 , barPos.y , 0)

	return Vector(StartPos.x, StartPos.y, 0), Vector(EndPos.x, EndPos.y, 0)
end

function DrawIndicator(unit, health)
	local SPos, EPos = GetHPBarPos(unit)
	local barlenght = EPos.x - SPos.x
	local Position = SPos.x + (health / unit.maxHealth) * barlenght
	if Position < SPos.x then
		Position = SPos.x
	end
	DrawText("|", 13,  math.floor(Position),  math.floor(SPos.y+10), ARGB(255,0,255,0))
end

function DrawOnHPBar(unit, health)
	local Pos = GetHPBarPos(unit)
	if health < 0 then
		DrawCircle2(unit.x, unit.y, unit.z, 100, ARGB(255, 255, 0, 0))	
		DrawText("HP: "..health,13, math.floor(Pos.x), (Pos.y+5), ARGB(255,255,0,0))
	else
		DrawText("HP: "..health,13,  math.floor(Pos.x),  math.floor(Pos.y+5), ARGB(255,0,255,0))
	end
end


function DrawCircleNextLvl(x, y, z, radius, width, color, chordlength)
	radius = radius or 300
	quality = math.max(8,math.floor(180/math.deg((math.asin((chordlength/(2*radius)))))))
	quality = 2 * math.pi / quality
	radius = radius*.92
	local points = {}
	for theta = 0, 2 * math.pi + quality, quality do
		local c = WorldToScreen(D3DXVECTOR3(x + radius * math.cos(theta), y, z - radius * math.sin(theta)))
		points[#points + 1] = D3DXVECTOR2(c.x, c.y)
	end
	DrawLines2(points, width or 1, color or 4294967295)
end


function DrawCircle2(x, y, z, radius, color)
    local vPos1 = Vector(x, y, z)
    local vPos2 = Vector(cameraPos.x, cameraPos.y, cameraPos.z)
    local tPos = vPos1 - (vPos1 - vPos2):normalized() * radius
    local sPos = WorldToScreen(D3DXVECTOR3(tPos.x, tPos.y, tPos.z))

    if OnScreen({ x = sPos.x, y = sPos.y }, { x = sPos.x, y = sPos.y })  then
        DrawCircleNextLvl(x, y, z, radius, 1, color, 75)	
    end
end

function OnDraw()
	if Menu.drawing.drawQ then
		DrawCircle2(myHero.x,myHero.y,myHero.z,Qrange,ARGB(255, 0, 255, 0))
	end
	
	if Menu.drawing.drawR then
		DrawCircleMinimap(myHero.x,myHero.y,myHero.z,Rrange)
	end
	
	if Menu.drawing.drawRN and CastingUltimate then
		DrawCircle(myHero.x,myHero.y,myHero.z,Rrange,ARGB(255, 0, 255, 0))
	end
	
		--[[Killable text tracker]]
	if Menu.drawing.health then
		for i=1, heroManager.iCount do
			local enemy = heroManager:GetHero(i)
			if ValidTarget(enemy) then
				if DamageToHeros[i] ~= nil then
					RemainingHealth = enemy.health - DamageToHeros[i]
				end
				if RemainingHealth ~= nil then
					DrawOnHPBar(enemy, math.floor(RemainingHealth))
					DrawIndicator(enemy, math.floor(RemainingHealth))
				end
			end
		end
	end
end

function CheckStunnedTargets()
	for o, enemy in ipairs(GetEnemyHeroes()) do
		for i = 1, enemy.buffCount do
			buff = enemy:getBuff(i)
			if buff.valid then 
				if buff.name == "Stun" then
					local found = false
					for _, stunned in ipairs(StunnedTargets) do
						if stunned.hero.charName == enemy.charName then
							found = true
						end
					end
					if not found then
						table.insert(StunnedTargets, {endTick = GetInGameTimer()*1000 + (buff.endT-buff.startT)*1000, hero = enemy, position = Vector(enemy.x,0,enemy.z)})
					end
				end
			end
		end
	end
  
	i=1
	while i <= #StunnedTargets do
		if (StunnedTargets[i].endTick > GetInGameTimer()*1000) then
			local RemainingTime = StunnedTargets[i].endTick - GetInGameTimer()*1000
			local enemy = StunnedTargets[i].hero
			local MinTime = ( (Qradius) / enemy.ms) * 1000
			
			for i, enemy2 in ipairs(GetEnemyHeroes()) do
				if enemy2.charName == enemy.charName then
					enemy = enemy2
				end
			end
			
			if ((RemainingTime + MinTime) > (GetDistance(enemy)/Qspeed)*1000) then --only cast W if enemy cant escape
				ProdictionQCallback2(enemy, enemy, _Q)
			end
			 i = i + 1
		else
			 table.remove(StunnedTargets, i)
		end
	end
end


function OnGainBuff(unit, buff)
	if ValidTarget(unit) and (unit.type == 'obj_AI_Hero') and (buff.type == BUFF_STUN or buff.type == BUFF_ROOT or buff.type == BUFF_KNOCKUP or buff.type == BUFF_SUPPRESS) then
		table.insert(StunnedTargets, {endTick = GetInGameTimer()*1000 + (buff.duration)*1000, hero = unit, position = Vector(unit.x,0,unit.z)})
	end
	
	if unit.isMe and buff.name:find("pickacard_tracker") then
		Status = SELECTING
	end
	
	if unit.isMe and buff.name:find("destiny") then
		CastingUltimate = true
	end
end

function OnLoseBuff(unit, buff)
	if unit.isMe and buff.name:find("pickacard_tracker") then --Card Locked
		Status = SELECTED
		CardColor = NONE
		ToSelect = NONE
		Sure = false
	elseif unit.isMe and buff.name:find("destiny") and Menu.spells.R.auto then --Card Locked
		ToSelect = NONE
		CastSpell(_W)
	end
	
	if unit.isMe and buff.name:find("destiny") then
		CastingUltimate = false
	end
end

function OnRecvPacket(p)
	if p.header == 0x17 then
		p.pos = 1
		local NId = p:DecodeF()
		if NId == myHero.networkID then
			p.pos = 7
			local b = p:Decode1()
			if b == 0x52 then--red
				if CardColor ~= NONE and CardColor ~= RED then
					Sure = true
				end
				CardColor = RED
			elseif b == 0x47 then--yellow
				if CardColor ~= NONE and CardColor ~= YELLOW then
					Sure = true
				end
				CardColor = YELLOW
			elseif b == 0x42 then--blue
				if CardColor ~= NONE and CardColor ~= BLUE then
					Sure = true
				end
				CardColor = BLUE
			end
		end
	end
end

function OnSendPacket(p)
	if BlockAttacks then
		local packet = Packet(p)
		if (packet:get('name') == 'S_MOVE') and ((packet:get('type') == 3) or (packet:get('type') == 7)) then
			packet:block()
		end
	end
end

--EOS--

